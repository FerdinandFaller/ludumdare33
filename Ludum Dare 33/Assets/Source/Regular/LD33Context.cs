﻿using de.FF.Utilities;
using UnityEngine;

namespace de.FF.LD33
{
    public class LD33Context : SignalContext
    {
        public LD33Context (MonoBehaviour view, bool autoStartup) : base(view, autoStartup)
        {
        }

        protected override void mapBindings()
        {
            // Injections
            injectionBinder.Bind<ILD33Model>().To<DefaultLD33Model>().ToSingleton();

            // Mediations
            mediationBinder.Bind<MainMenuView>().To<MainMenuMediator>();
            mediationBinder.Bind<VersionView>().To<VersionMediator>();

            // Commands
            commandBinder.Bind<StartSignal>().To<StartCommand>().Once();
            commandBinder.Bind<ShowHideGuiViewSignal>().To<ShowHideGuiViewCommand>();
            commandBinder.Bind<SetVersionSignal>().To<SetVersionCommand>();
        }
    }
}