﻿using de.FF.Utilities;

namespace de.FF.LD33
{
    public interface ILD33Model
    {
        Version AppVersion { get; set; }
    }
}