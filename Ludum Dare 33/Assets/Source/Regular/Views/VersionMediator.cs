using de.FF.Utilities;
using strange.extensions.mediation.impl;

namespace de.FF.LD33
{
    public class VersionMediator : Mediator
    {
        [Inject]
        public VersionView VersionView { get; set; }
        [Inject]
        public SetVersionSignal SetVersionSignal { get; set; }


        public override void OnRegister()
        {
            base.OnRegister();

            SetVersionSignal.Dispatch(new Version(VersionView.Major, VersionView.Minor, VersionView.Revision));
        }
    }
}