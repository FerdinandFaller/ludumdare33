using strange.extensions.mediation.impl;

namespace de.FF.LD33
{
    public class VersionView : View
    {
        public int Major;
        public int Minor;
        public int Revision;
    }
}