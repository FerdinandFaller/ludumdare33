using strange.extensions.mediation.impl;

namespace de.FF.LD33
{
    public class MainMenuMediator : Mediator
    {
        [Inject]
        public MainMenuView MainMenuView { get; set; }
        [Inject]
        public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }


        public override void OnRegister()
        {
            base.OnRegister();
            
            // Register listeners here
        }

        public override void OnRemove()
        {
            base.OnRemove();

            // Remove listeners here
        }
    }
}