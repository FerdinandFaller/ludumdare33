﻿using System;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

namespace de.FF.LD33
{
    public class LD33Root : ContextView
    {
        public Dictionary<Type, GameObject> GuiViews = new Dictionary<Type, GameObject>();

        public bool TestMode;
        public GameObject MainMenuPanel;

        
        void Awake()
        {
            context = new LD33Context(this, true);

            if (!TestMode)
            {
                if(MainMenuPanel != null) GuiViews.Add(typeof(MainMenuView), MainMenuPanel);

                foreach (var panels in GuiViews.Values)
                {
                    panels.SetActive(false);
                }
            }
            
            context.Start();
        }
    }
}