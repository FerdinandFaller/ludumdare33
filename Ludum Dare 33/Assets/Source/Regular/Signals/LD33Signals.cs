﻿using System;
using strange.extensions.signal.impl;
using Version = de.FF.Utilities.Version;

namespace de.FF.LD33
{
    public class StartSignal : Signal { }

    public class ShowHideGuiViewSignal : Signal<Type, bool> { }

    public class SetVersionSignal : Signal<Version> { }
}