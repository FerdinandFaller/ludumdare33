using System;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;

namespace de.FF.LD33
{
    public class ShowHideGuiViewCommand : Command
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject ContextView { get; set; }

        [Inject]
        public Type GuiViewType { get; set; }

        [Inject]
        public bool ShouldBeVisible { get; set; }


        public override void Execute()
        {
            var root = ContextView.GetComponent<LD33Root>();

            if (root.GuiViews.Count <= 0) return;
            GameObject panel = root.GuiViews[GuiViewType];
            panel.SetActive(ShouldBeVisible);
        }
    }
}
