using de.FF.Utilities;
using strange.extensions.command.impl;

namespace de.FF.LD33
{
    public class SetVersionCommand : Command
    {
        [Inject]
        public ILD33Model Model { get; set; }

        [Inject]
        public Version AppVersion { get; set; }


        public override void Execute()
        {
            Model.AppVersion = AppVersion;
        }
    }
}