﻿using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;

namespace de.FF.LD33
{
    public class StartCommand : Command
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject ContextView { get; set; }

        [Inject]
        public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }


        public override void Execute()
        {
            if (ContextView.GetComponent<LD33Root>().TestMode) return;

            Application.LoadLevelAdditive("Main");

            ShowHideGuiViewSignal.Dispatch(typeof(MainMenuView), true);
        }   
    }
}