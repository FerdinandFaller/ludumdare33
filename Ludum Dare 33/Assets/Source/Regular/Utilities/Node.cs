namespace de.FF.Utilities
{
    public class Node : NodeBase
    {
        public Node(uint x, uint y, uint z) : base(x, y, z)
        {
        }

        public Node(Index3D index) : base(index)
        {
        }
    }
}