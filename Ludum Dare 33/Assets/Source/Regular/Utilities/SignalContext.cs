﻿using de.FF.LD33;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace de.FF.Utilities
{
    public class SignalContext : MVCSContext
    {
        /**
         * Constructor
         */
        protected SignalContext(MonoBehaviour contextView, bool autoStartup) : base(contextView, autoStartup)
        {
        }

        protected override void addCoreComponents()
        {
            base.addCoreComponents();

            // bind signal command binder
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
        }

        public override void Launch()
        {
            base.Launch();
            Signal startSignal = injectionBinder.GetInstance<StartSignal>();
            startSignal.Dispatch();
        }
    }
}