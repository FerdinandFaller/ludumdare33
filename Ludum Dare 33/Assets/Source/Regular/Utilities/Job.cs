using System.Collections;
using System.Collections.Generic;

namespace de.FF.Utilities
{
    public class Job
    {
        public event System.Action<bool> JobComplete;

        public bool Running { get; private set; }

        public bool Paused { get; private set; }


        private readonly IEnumerator _coroutine;
        private bool _jobWasKilled;
        private Stack<Job> _childJobStack;


        #region constructors

        public Job(IEnumerator coroutine, bool shouldStart = true)
        {
            _coroutine = coroutine;

            if (shouldStart)
                Start();
        }

        #endregion


        #region static Job makers

        public static Job Make(IEnumerator coroutine)
        {
            return new Job(coroutine);
        }


        public static Job Make(IEnumerator coroutine, bool shouldStart)
        {
            return new Job(coroutine, shouldStart);
        }

        #endregion


        #region public API

        public Job CreateAndAddChildJob(IEnumerator coroutine)
        {
            var j = new Job(coroutine, false);
            AddChildJob(j);
            return j;
        }


        public void AddChildJob(Job childJob)
        {
            if (_childJobStack == null)
                _childJobStack = new Stack<Job>();
            _childJobStack.Push(childJob);
        }


        public void RemoveChildJob(Job childJob)
        {
            if (_childJobStack.Contains(childJob))
            {
                var childStack = new Stack<Job>(_childJobStack.Count - 1);
                var allCurrentChildren = _childJobStack.ToArray();
                System.Array.Reverse(allCurrentChildren);

                foreach (var j in allCurrentChildren)
                {
                    if (j != childJob)
                        childStack.Push(j);
                }

                // assign the new stack
                _childJobStack = childStack;
            }
        }


        public void Start()
        {
            _jobWasKilled = false;
            Paused = false;
            Running = true;
            JobManager.Instance.StartCoroutine(DoWork());
        }


        public IEnumerator StartAsCoroutine()
        {
            Running = true;
            yield return JobManager.Instance.StartCoroutine(DoWork());
        }


        public void Pause()
        {
            Paused = true;
        }


        public void Unpause()
        {
            Paused = false;
        }


        public void Kill()
        {
            _jobWasKilled = true;
            Running = false;
            Paused = false;
        }


        public void Kill(float delayInSeconds)
        {
            var delay = (int)(delayInSeconds * 1000);
            new System.Threading.Timer(obj =>
            {
                lock (this)
                {
                    Kill();
                }
            }, null, delay, System.Threading.Timeout.Infinite);
        }

        #endregion


        private IEnumerator DoWork()
        {
            // null out the first run through in case we start paused
            yield return null;

            while (Running)
            {
                if (Paused)
                {
                    yield return null;
                }
                else
                {
                    // run the next iteration and stop if we are done
                    if (_coroutine.MoveNext())
                    {
                        yield return _coroutine.Current;
                    }
                    else
                    {
                        // run our child jobs if we have any
                        if (_childJobStack != null)
                            yield return JobManager.Instance.StartCoroutine(RunChildJobs());
                        Running = false;
                    }
                }
            }

            // fire off a complete event
            if (JobComplete != null)
                JobComplete(_jobWasKilled);
        }


        private IEnumerator RunChildJobs()
        {
            if (_childJobStack != null && _childJobStack.Count > 0)
            {
                do
                {
                    Job childJob = _childJobStack.Pop();
                    yield return JobManager.Instance.StartCoroutine(childJob.StartAsCoroutine());
                }
                while (_childJobStack.Count > 0 && Running);
            }
        }
    }
}