using System;
using System.Collections.Generic;

namespace de.FF.Utilities
{
    public interface INode : IEquatable<INode>
    {
        Index3D Index { get; }
        List<Index3D> ConnectedIndexes { get; }

        void AddConnection(Index3D index);
        void RemoveConnection(Index3D index);
    }
}