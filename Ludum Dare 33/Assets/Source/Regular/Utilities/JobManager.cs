using UnityEngine;

namespace de.FF.Utilities
{
    // JobManager is just a proxy object so we have a launcher for the coroutines
    public class JobManager : MonoBehaviour
    {
        // only one JobManager can exist. We use a singleton pattern to enforce this.
        static JobManager _instance;
        public static JobManager Instance
        {
            get
            {
                if (!_instance)
                {
                    // check if an JobManager is already available in the scene graph
                    _instance = FindObjectOfType(typeof(JobManager)) as JobManager;

                    // nope, create a new one
                    if (!_instance)
                    {
                        var obj = new GameObject("JobManager");
                        _instance = obj.AddComponent<JobManager>();
                    }
                }

                return _instance;
            }
        }


        void OnApplicationQuit()
        {
            // release reference on exit
            _instance = null;
        }
    }
}