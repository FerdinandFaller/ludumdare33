using System;
using System.Collections.Generic;
using UnityEngine;

namespace de.FF.Utilities
{
    public class PoolNotInitializedException : Exception
    {
        
    }

    public abstract class ObjectPool<T>
    {
        public event Action<IPoolable<T>> PoolableWasActivatedEvent;

        protected int InitialPoolSize;
        protected List<IPoolable<T>> FreePool;
        protected List<IPoolable<T>> UsedPool;
        protected bool IsInitialized;

        /// <summary>
        /// Percentage of FreeCount+UsedCount of Objects in the FreePool that should be kept when doing a CleanUp.
        /// </summary>
        private readonly float _keepFreeAtCleanUpPercentage;

        private readonly int _amountOfResetsUntilCleanUp;
        private int _resetCounter;
        

        public int FreeCount
        {
            get
            {
                return FreePool.Count;
            }
        }

        public int UsedCount
        {
            get
            {
                return UsedPool.Count;
            }
        }


        protected ObjectPool(int initialPoolSize, float keepFreeAtCleanUpPercentage = 0.2f, int amountOfResetsUntilCleanUp = 100)
        {
            FreePool = new List<IPoolable<T>>(initialPoolSize);
            UsedPool = new List<IPoolable<T>>(initialPoolSize);
            
            _keepFreeAtCleanUpPercentage = Mathf.Clamp01(keepFreeAtCleanUpPercentage);
            _amountOfResetsUntilCleanUp = amountOfResetsUntilCleanUp;
        }

        public IPoolable<T> GetFreeObject()
        {
            if(!IsInitialized) throw new PoolNotInitializedException();

            IPoolable<T> returnVar;

            if (FreePool.Count <= 0)
            {
                returnVar = MakeNewPoolObject();
                UsedPool.Add(returnVar);
            }
            else
            {
                returnVar = FreePool[0];
                FreePool.RemoveAt(0);

                UsedPool.Add(returnVar);
            }

            if (PoolableWasActivatedEvent != null)
            {
                PoolableWasActivatedEvent(returnVar);
            }

            return returnVar;
        }

        public void ResetAllObjects()
        {
            foreach (var obj in UsedPool)
            {
                obj.Reset();
            }

            CleanUp();
        }

        protected virtual void Init(int initialPoolSize)
        {
            InitialPoolSize = initialPoolSize;

            for (var i = 0; i < InitialPoolSize; i++)
            {
                FreePool.Add(MakeNewPoolObject());
            }

            IsInitialized = true;
        }

        /// <summary>
        /// Removes all traces of the Pool as if it never was initialized. You have to initialize it again if you want to use it again. 
        /// </summary>
        public virtual void Clear()
        {
            FreePool.Clear();
            UsedPool.Clear();

            IsInitialized = false;
        }

        protected int CalculateCleanUpAmount()
        {
            var keepFreeAmount = (FreeCount + UsedCount) * _keepFreeAtCleanUpPercentage;
            var returnVar = Mathf.Max(FreeCount - keepFreeAmount, 0);
            if(returnVar >= 1) Debug.Log("FreeCount: " + FreeCount + " UsedCount: " + UsedCount + " TotalCount: " + (FreeCount+UsedCount) + " CleanUpAmount: " + returnVar);
            return (int) returnVar;
        }

        protected void OnPoolableWasResetted(IPoolable<T> obj)
        {
            if (UsedPool.Contains(obj))
            {
                UsedPool.Remove(obj);
                FreePool.Add(obj);

                _resetCounter++;
                Debug.Log("Resetcounter: " + _resetCounter);

                if (_resetCounter >= _amountOfResetsUntilCleanUp)
                {
                    _resetCounter = 0;
                    CleanUp();
                }
            }
        }

        protected abstract void CleanUp();

        /// <summary>
        /// Creates a new Object for the FreePool and subscribes for the WasResettedEvent.
        /// </summary>
        /// <returns>The newly created Object</returns>
        protected abstract IPoolable<T> MakeNewPoolObject();
    }
}
