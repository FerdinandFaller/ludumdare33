using System;

namespace de.FF.Utilities
{
    public abstract class Poolable<T> : IPoolable<T>
    {
        protected Poolable(ObjectPool<T> pool)
        {
            pool.PoolableWasActivatedEvent -= OnObjectPoolPoolableWasActivated;
            pool.PoolableWasActivatedEvent += OnObjectPoolPoolableWasActivated;
        }

        public abstract void OnObjectPoolPoolableWasActivated(IPoolable<T> obj);
        public abstract void Reset();
        public abstract event Action<IPoolable<T>> WasResettedEvent;
        public abstract T GetObject();
    }
}