using System.Globalization;

namespace de.FF.Utilities
{
    public class Version
    {
        public static Version Instance { get; private set; }

        public int Major { get; private set; }
        public int Minor { get; private set; }
        public int Revision { get; private set; }


        public Version(int major, int minor, int revision)
        {
            Instance = this;

            Major = major;
            Minor = minor;
            Revision = revision;
        }

        public Version()
        {
            Major = 0;
            Minor = 0;
            Revision = 0;
        }

        public override string ToString()
        {
            return "v" + Major.ToString(CultureInfo.InvariantCulture) + "." + Minor.ToString(CultureInfo.InvariantCulture) + "." + Revision.ToString(CultureInfo.InvariantCulture);
        }

        public static bool operator < (Version a, Version b)
        {
            return  a.Major < b.Major ||
                    (a.Major == b.Major && a.Minor < b.Minor) || 
                    (a.Major == b.Major && a.Minor == b.Minor && a.Revision < b.Revision);
        }

        public static bool operator > (Version a, Version b)
        {
            return  a.Major > b.Major || 
                    (a.Major == b.Major && a.Minor > b.Minor) || 
                    (a.Major == b.Major && a.Minor == b.Minor && a.Revision > b.Revision);
        }
    }
}