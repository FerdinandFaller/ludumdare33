using System.Collections.Generic;

namespace de.FF.Utilities
{
    public abstract class NodeBase : INode
    {
        private Index3D _index;

        public Index3D Index
        {
            get { return _index; }
            private set { _index = value; }
        }

        public List<Index3D> ConnectedIndexes { get; set; }


        protected NodeBase()
        {
            Index = new Index3D();
            ConnectedIndexes = new List<Index3D>();
        }

        protected NodeBase(uint x, uint y, uint z) : this()
        {
            _index.X = x;
            _index.Y = y;
            _index.Z = z;
        }

        protected NodeBase(Index3D index) : this(index.X, index.Y, index.Z)
        {
        }

        public void AddConnection(Index3D index)
        {
            if (ConnectedIndexes.Contains(index)) return;
            ConnectedIndexes.Add(index);
        }

        public void RemoveConnection(Index3D index)
        {
            if (!ConnectedIndexes.Contains(index)) return;
            ConnectedIndexes.Remove(index);
        }

        public bool Equals(INode other)
        {
            return Index.Equals(other.Index) && ConnectedIndexes.Equals(other.ConnectedIndexes);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is INode && Equals((INode)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Index.GetHashCode() + ConnectedIndexes.GetHashCode();
            }
        }

        public static bool operator ==(NodeBase lhs, NodeBase rhs)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(lhs, rhs))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)lhs == null) || ((object)rhs == null))
            {
                return false;
            }

            return lhs.Index == rhs.Index && lhs.ConnectedIndexes == rhs.ConnectedIndexes;
        }

        public static bool operator !=(NodeBase lhs, NodeBase rhs)
        {
            return !(lhs == rhs);
        }

        public override string ToString()
        {
            return Index + ": " + ConnectedIndexes;
        }
    }
}