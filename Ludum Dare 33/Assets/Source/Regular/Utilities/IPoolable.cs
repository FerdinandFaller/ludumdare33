using System;

namespace de.FF.Utilities
{
    public interface IPoolable<T>
    {
        void Reset();
        event Action<IPoolable<T>> WasResettedEvent;

        T GetObject();

        void OnObjectPoolPoolableWasActivated(IPoolable<T> obj);
    }
}