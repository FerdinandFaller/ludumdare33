using System;

namespace de.FF.Utilities
{
    public struct Index3D : IEquatable<Index3D>
    {
        public static Index3D Invalid { get { return new Index3D(uint.MaxValue, uint.MaxValue, uint.MaxValue);} }

        public uint X;
        public uint Y;
        public uint Z;


        public Index3D(uint x, uint y, uint z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public bool Equals(Index3D other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Index3D && Equals((Index3D)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)X * 397) ^ (int)Y ^ (int)Z;
            }
        }

        public static bool operator ==(Index3D lhs, Index3D rhs)
        {
            return lhs.X == rhs.X && lhs.Y == rhs.Y && lhs.Z == rhs.Z;
        }

        public static bool operator !=(Index3D lhs, Index3D rhs)
        {
            return lhs.X != rhs.X || lhs.Y != rhs.Y || lhs.Z != rhs.Z;
        }

        public override string ToString()
        {
            return X + " , " + Y + " , " + Z;
        }
    }
}