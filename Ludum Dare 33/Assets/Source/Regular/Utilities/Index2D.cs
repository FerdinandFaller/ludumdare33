namespace de.FF.Utilities
{
    public struct Index2D
    {
        public static Index2D Invalid { get { return new Index2D(uint.MaxValue, uint.MaxValue); } }

        public uint X;
        public uint Y;


        public Index2D(uint x, uint y)
        {
            X = x;
            Y = y;
        }

        public bool Equals(Index2D other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Index2D && Equals((Index2D)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)X * 397) ^ (int)Y;
            }
        }

        public static bool operator ==(Index2D lhs, Index2D rhs)
        {
            return lhs.X == rhs.X && lhs.Y == rhs.Y;
        }

        public static bool operator !=(Index2D lhs, Index2D rhs)
        {
            return lhs.X != rhs.X || lhs.Y != rhs.Y;
        }

        public override string ToString()
        {
            return X + " , " + Y;
        }
    }
}